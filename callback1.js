const boards = require('./data/boards.json');

function getBoard(boardID, callback) {

    setTimeout(() => {
        let board = boards.filter((board) => board.id === boardID);

        if(board.length === 1){
            callback(null,board[0]);
        } else {
            callback(new Error('Board not found'));
        }
        
    }, 2 * 1000);
}

module.exports = getBoard;
