const lists = require('./data/lists.json');

function getBoardsList(boardID, callback) {

    setTimeout(() => {
        let boardList = lists[boardID];

        if(boardList !== undefined){
            callback(null,boardList);
        } else {
            callback(new Error('Board List not found'));
        }
        
    },2 * 1000);
}

module.exports = getBoardsList;