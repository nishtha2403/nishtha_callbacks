const cards = require('./data/cards.json');

function getCardSet(listID, callback) {
    
    setTimeout(() => {
        let cardSet = cards[listID];

        if(cardSet !== undefined){
            callback(null,cardSet);
        } else {
            callback(new Error('Card Set not found'));
        }

    }, 2 * 1000);
}

module.exports = getCardSet;