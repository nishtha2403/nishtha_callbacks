const boards = require('./data/boards.json');
const getBoard = require('./callback1');
const getBoardList = require('./callback2');
const getCardSet = require('./callback3');

function getOneCardSet(boardName,listName,callback) {

    setTimeout(() => {

        let boardData = boards.filter((board) => board.name == boardName)[0];
        if(boardData === undefined){
            callback(new Error('Board not found'));
        } else {
            let boardID = boardData.id;
            getBoard(boardID,(err,board) => {
                if(err){
                    callback(err);
                } else {
                    console.log(board);
    
                    getBoardList(boardID,(err,boardList) => {
                        if(err) {
                            callback(err);
                        } else {
                            let listData = boardList.filter((value) => value.name === listName)[0];
                            
                            if(listData === undefined){
                                callback(new Error('Listname not found'));
                            } else {
                                getCardSet(listData.id, (err,cardSet) => {   
                                    if(err){
                                        callback(new Error('Card set not found'));
                                    } else {
                                        callback(null,cardSet);
                                    }
                                });
                            }
                        }
                    });
                } 
            });
        }

    },2 * 1000);
}

module.exports = getOneCardSet;