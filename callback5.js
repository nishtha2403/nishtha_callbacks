const boards = require('./data/boards.json');
const getBoard = require('./callback1');
const getBoardList = require('./callback2');
const getCardSet = require('./callback3');

function getMultipleCardSets(callback,boardName, ...listNames) {

    setTimeout(() => {
        let cardSetArray = [];
        let counter = 0;
        
        if(listNames.length > 0){

            let boardData = boards.filter((board) => board.name === boardName)[0];
            if(boardData === undefined){
                callback(new Error('Board not found'));
            } else {
                let boardID = boardData.id;

                getBoard(boardID, (err,board) => {
                    if(err){
                        callback(err);
                    } else {
                        console.log(board);
    
                        getBoardList(boardID, (err,boardList) => {
                            if(err) {
                                callback(err);
                            } else {
                                let listIDArray = [];

                                listNames.forEach((listName) => {
                                    let listData = boardList.filter((list) => list.name === listName)[0];
                                    if(listData !== undefined){
                                        listIDArray.push(listData.id);
                                    }else {
                                        listIDArray.push(null);
                                    }
                                });
        
                                listIDArray.forEach((listID,index) => {
                                    getCardSet(listID, (err,cardSet) => {
                                        if(err) {
                                            callback(err);
                                        } else {
                                            cardSetArray[index] = cardSet;
                                            counter += 1;
                                            if(counter === listNames.length){
                                                callback(null,cardSetArray);
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            }    
        } else {
            callback(new Error('No List names'));
        }
    }, 2 * 1000);
}

module.exports = getMultipleCardSets;