const boards = require('./data/boards.json');
const getBoard = require('./callback1');
const getBoardsList = require('./callback2');
const getCardSet = require('./callback3');

function getAllCardSet(boardName,callback) {

    setTimeout(() => {
        
        let boardData = boards.filter((board) => board.name === boardName)[0];
        if(boardData === undefined){
            callback(new Error('Board not found'));
        } else {
            let boardID = boardData.id;
            getBoard(boardID,(err,board) => {
                if(err){
                    callback(err);
                } else {
                    console.log(board);
    
                    getBoardsList(boardID, (err,boardList) => {
                        if(err){
                            callback(err);
                        } else {
                            let allCardSet = [];
                            let counter = 0;
                            boardList.forEach((list) => {
                                let listID = list.id;   
        
                                getCardSet(listID, (err,cardSet) => {
                                    if(!err){
                                        allCardSet.push(cardSet);
                                    }
                                    counter += 1;   
                                    if(counter === boardList.length){
                                        callback(null,allCardSet);
                                    }
                                });
                            });
                        }
                    }); 
                }
            });
        } 
    }, 2 * 1000);
}

module.exports = getAllCardSet;