const getBoard = require('../callback1');

let boardID = 'mcu453ed';

getBoard(boardID, (err,board) => {
    if(err){
        console.error(err);
    } else {
        console.log(board);
    }
});

let incorrectBoardID = 'mcu5678f';

getBoard(incorrectBoardID, (err,board) => {

    if(err){
        console.error(err);
    }else{
        console.log(board);
    }  
});
