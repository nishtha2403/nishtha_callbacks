const getBoardsList = require('../callback2');

let boardID = 'abc122dc';

getBoardsList(boardID, (err,boardList) => {

    if(err){
        console.error(err);
    } else {
        console.log(boardList);
    }
    
});

let incorrectBoardID = '2403abdc';

getBoardsList(incorrectBoardID, (err,boardList) => {

    if(err){
        console.error(err);
    } else {
        console.log(boardList);
    }

});



