const getCardSet = require('../callback3');

let listID = 'qwsa221';

getCardSet(listID, (err,cardSet) => {

    if(err){
        console.error(err);
    } else {
        console.log(cardSet);
    }
})

let incorrectListID = "hhww442";

getCardSet(incorrectListID, (err,cardSet) => {

    if(err){
        console.error(err);
    } else {
        console.log(cardSet);
    }
})