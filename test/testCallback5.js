const getMultipleCardSets = require('../callback5');

let boardName = "Thanos";
let listName1 = "Mind";
let listName2 = "Space";

function callback(err,cardSetArray) {
    if(err){
        console.error(err);
    } else {
        console.log(cardSetArray);
    }
}

getMultipleCardSets(callback,boardName,listName1,listName2);

// getMultipleCardSets((cardSetArray) => {
//     console.log(cardSetArray);
// });